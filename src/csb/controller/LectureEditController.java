package csb.controller;

import static csb.CSB_PropertyType.REMOVE_ITEM_MESSAGE;
import csb.data.Course;
import csb.data.CourseDataManager;
import csb.data.Lecture;


import csb.gui.CSB_GUI;
import csb.gui.LectureItemDialog;
import csb.gui.MessageDialog;

import csb.gui.YesNoCancelDialog;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author McKillaGorilla
 */
public class LectureEditController {
    LectureItemDialog ld;
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    
    public LectureEditController(Stage initPrimaryStage, Course course, MessageDialog initMessageDialog, YesNoCancelDialog initYesNoCancelDialog) {
        ld = new LectureItemDialog(initPrimaryStage, course, initMessageDialog);
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
    }

    // THESE ARE FOR lecture
    
    public void handleAddLectureItemRequest(CSB_GUI gui) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        ld.showAddLectureItemDialog();
        
        // DID THE USER CONFIRM?
        if (ld.wasCompleteSelected()) {
            // GET THE SCHEDULE ITEM
            Lecture lectureItem = ld.getLecture();
            
            // AND ADD IT AS A ROW TO THE TABLE
            course.addLecture(lectureItem);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }
    
    public void handleEditLectureItemRequest(CSB_GUI gui, Lecture itemToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        ld.showEditScheduleItemDialog(itemToEdit);
        
        // DID THE USER CONFIRM?
        if (ld.wasCompleteSelected()) {
            // UPDATE THE SCHEDULE ITEM
            Lecture l = ld.getLecture();
            itemToEdit.setTopic(l.getTopic());
            itemToEdit.setSessions(l.getSessions());
            //itemToEdit.setLink(si.getLink());
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }        
    }
    
    public void handleRemoveLectureItemRequest(CSB_GUI gui, Lecture itemToRemove) {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) { 
            gui.getDataManager().getCourse().removeLecture(itemToRemove);
        }
    }

    public void handleUpLectureItemRequest(CSB_GUI gui, Lecture itemToMove) {
        int from = gui.getDataManager().getCourse().getLectures().indexOf(itemToMove);
        gui.getDataManager().getCourse().swapLecturePosition(from-1, from);
        
    }
   public void handleDownLectureItemRequest(CSB_GUI gui, Lecture itemToMove) {
        int from = gui.getDataManager().getCourse().getLectures().indexOf(itemToMove);
        gui.getDataManager().getCourse().swapLecturePosition(from, from+1);
        
    }
}